// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <algorithm>  // min, max

#include "Collatz.h"

using namespace std;


int lazy_cache[1000000];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}


// ------------
// collatz_eval_helper
// ------------

int collatz_eval_helper(int val) {
    // return cycle length of val
    // add val and result to cache if it doesn't already exist
    assert(0 < val && val < 1000000);

    if (lazy_cache[val] != 0)
        return lazy_cache[val];
    long long modified_value = val;
    int count = 1;
    while(modified_value > 1) {
        if (modified_value % 2 == 1) {
            modified_value = (3 * modified_value + 1) / 2;
            ++count;
        }
        else {
            modified_value = modified_value / 2;
        }
        ++count;
    }

    assert(count > 0);
    lazy_cache[val] = count;
    return count;
}


// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    // iterate through values and return max cycle length
    assert(0 < i && i < 1000000);
    assert(0 < j && j < 1000000);

    int start_val = std::min(i, j);
    int end_val = std::max(i, j);
    int result = 0;
    for(int index = start_val; index < end_val + 1; index++) {
        int val = collatz_eval_helper(index);
        result = std::max(result, val);
    }

    assert(result > 0);
    return result;
}


// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
