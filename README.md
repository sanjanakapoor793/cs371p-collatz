# CS371p: Object-Oriented Programming Collatz Repo

* Name: Sanjana Kapoor

* EID: sk45257

* GitLab ID: sanjanakapoor793

* HackerRank ID: sanjanakapoor1

* Git SHA: 5599f81517a3782ac4feca5864cedeb0c9dab9d5

* GitLab Pipelines: https://gitlab.com/sanjanakapoor793/cs371p-collatz/pipelines

* Estimated completion time: 5 Hours

* Actual completion time: 4.5 hours

* Comments: N/A